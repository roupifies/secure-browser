const sqlite3 = require('sqlite3').verbose();
const db = new sqlite3.Database('whitelist.sqllite');
const VISIT_THRESHOLD = 3;

const update = (url) => {
    return new Promise((resolve) => db.serialize(() => {
        exist(url).then(({exist}) => {
            if (exist) {
                countVisit(url).then(value => {
                    const updatedCount = value + 1;
                    const sqlToUpdateCount = `UPDATE whitelist SET countNo=${updatedCount} WHERE url = '${url}'`;
                    db.run(sqlToUpdateCount);
                });
            }
            resolve("updated");
        })
    }));
};

const shouldDisplay = url => new Promise(resolve =>
    db.serialize(() =>
        exist(url).then(({exist}) => {
            if (exist) countVisit(url).then(value => resolve(value <= VISIT_THRESHOLD));
            else resolve(false);
        })));

const exist = (url) => {
    let exits = `SELECT EXISTS(SELECT 1 FROM whitelist WHERE url = '${url}' LIMIT 1) as exist`;
    return new Promise(resolve => db.serialize(() => db.each(exits, (err, success) => resolve(success))));
};
const countVisit = (url) => {
    let countVisitSql = `SELECT url, countNo FROM whitelist WHERE url = '${url}'`;
    return new Promise(resolve => db.serialize(() => db.each(countVisitSql, (err, success) => resolve(success.countNo))));
};
module.exports = {
    update,
    shouldDisplay,
    countVisit,
};
