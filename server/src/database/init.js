const sqlite3 = require('sqlite3').verbose();
const db = new sqlite3.Database('whitelist.sqllite');

const initializeDb = async () => {
    const whitelistUrls = ["www.google.com", "www.facebook.com", "twitter.com"];
    db.serialize(function () {
        let sqlToCreateTable = "CREATE TABLE IF NOT EXISTS whitelist (url TEXT, countNo INTEGER)";
        db.run(sqlToCreateTable);
        db.each("SELECT count(url) as count from whitelist", (err, value) => {
            if (value.count === 0) {
                whitelistUrls.forEach(url => {
                    let sqlToInsert = `INSERT OR REPLACE INTO whitelist(url, countNo) VALUES ('${url}',0)`;
                    db.run(sqlToInsert);
                });
            }
            db.close();
        });

    });

};

module.exports = {
    initializeDb,
};
