const express = require('express');
const init = require('./database/init');
const curd = require('./database/curd');
const cors = require('cors');

const app = express();

app.use(express.urlencoded());
app.use(express.json());
app.use(cors());

app.post('/api/:url/visit', (request, response) => {
    let url = request.params.url;
    curd.update(url).then(() =>
        curd.shouldDisplay(url).then(shouldDisplay => response.json({shouldDisplay})));
});

app.get('/api/:url/shouldDisplay', (request, response) => {
    const url = request.params.url;
    curd.shouldDisplay(url).then(shouldDisplay => response.json({
        shouldDisplay: shouldDisplay
    }));
});

app.get('/api/:url/visitCount', (request, response) => {
    const url = request.params.url;
    curd.shouldDisplay(url).then(shouldDisplay => {
        if (shouldDisplay) {
            curd.countVisit(url).then(visitCount => response.json({
                visitCount: visitCount,
                shouldDisplay: true
            }));
        } else {
            response.json({
                visitCount: 0,
                shouldDisplay: false
            });
        }
    });

});

app.listen(4000, () => {
    init.initializeDb();
    console.log('Node server listening on http://localhost:4000');
});

