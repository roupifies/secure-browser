# What is it
I have created a chrome extension which shows a badge icon when the user navigates to whitelisted websites.
A pop up will be display with expected information about tracking website.
It only shows the badge 3 times.

# How to run
To create the extension run 

`npm run install-client && npm run build` and unpack the build folder into chrome browser extension i.e. chrome://extensions/

to run the backend run

`npm run install-server && npm run server`

# Technology:
I have used react/redux to create extension and use Nodejs and SQLite for the backend to persist the data.

# Extra tasks

- Store backend data in SQLite database instead of a file.
    - done! it stores the whitelisted URLs into the database. The listed URLs store in the init.js file.
    
- Display notification in the context of the page (e.g. a “ribbon” at the top of the screen).
    - I have used pop up notification to show the message as a ribbon at top of the page.
- Record number of times each user has seen a notification for each URL back to the
database (you can generate a user GUID either on the client or server).
    - I have store the visited times into database but did not get a chance to store it against each user.
    
# If I have more time    
- Support for multiple languages in notification.
    - it could be achieved by sending the translated message from backend to front-end
- Record number of times each user visit the website    
    - it could be achieved to generating a GUID on backend base on existing of a cookie.
- Manage whitelisted URLs    
    - it could be achieved to create Restful endpoint to allow admin to add/remove/update whitelisted URLs to the database 
    
